// Ejemplos de funcionamiento
// JS es un lenguaje débilmente tipado

4 + "7";    //47
4 * "7";    //28
2 + true;   // 3
false - 3;  //-3

// Existen dos tipos de coerciones
//    1. Implícitas (El lenguaje nos ayuda y cambia los tipos de datos para que coincidan)
//    2. Explícitas (Nosotros obligamos a los tipos de datos para que coincidan)

var a = 20;
var b = a + "";
var c = String(a);