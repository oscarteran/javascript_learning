// Se conoce como Hoisting al proceso de declarar funciones o variables
// antes que se procese cualquier tipo de código

// Solo funciona con versiones pasaba de JS

console.log(miNombre);

var miNombre;
miNombre = "Diego";
// Salida en Navegador: Undefined

mi_funcion();

function mi_funcion(){
    console.log('Hola ' + miNombre2);
}

miNombre2 = "Diego";

// Por buenas prácticas, todas las funciones deben ser declaradas al principio del código