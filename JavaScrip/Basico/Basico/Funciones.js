// Existen dos tipos de funciones

// Declarativas
function mi_funcion() {
    return 10;
}


// De expresion o funciones anonimas
var mi_funcion_2 = function(a, b){
    return a + b;
}


function Saludar(estudiante){
    console.log(estudiante);
}

// Template String o Plantilla de cadena de texto'
function Saludar(estudiante){
    console.log("Hola ${estudiante}");
}

