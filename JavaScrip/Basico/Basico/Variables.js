// Codigo de sintexis para JS
// Oscar Hernandez Teran 
// Jueves 02 de Febrero del 2023

// var es la representacion del lugar en memoria donde se va a guardar la información
var nombre = "Oscar";

// Existen dos maneras de variables

// Declaracion 
var edad;

// Inicializar 
edad = 30


var elementos = ["Computadora", "Celular"];
var persona = {
    nombre:"Diego",
    edad: 30
}


