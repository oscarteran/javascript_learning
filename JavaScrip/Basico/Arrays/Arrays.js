// Son objetos que a su vez contienen otras estructuras de datos primitivas

var frutas = ["Manzana", "Platano", "Cereza", "Fresa"];
console.log(frutas)           // Se imprime la longitud y todo el array con sus valores 
console.log(frutas.length)    // Longitud del array
console.log(frutas[0])        // Indexación de arrays, se inicializa en 0

// Métodos para mutar arrays
var mas_Frutas = frutas.push("Uvas")   // Añade elementos al final del array

var ultimo = frutas.pop()              // Elimina el ultimo elemento del array

var nueva_longitud = frutas.unshift("Naranjas")      // Agregamos un elemento al inicio del array

var borrar_Fruta = frutas.shift()      // Eliminar el primer elemento del array

var posicion = frutas.indexOf("Manzana") // Conoce la posición del elemento pasado