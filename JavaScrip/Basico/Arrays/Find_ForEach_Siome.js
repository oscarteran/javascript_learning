var articulos = [
    { nombre: "Bici", costo: 3000},
    { nombre: "TV", costo: 3500},
    { nombre: "Libro", costo: 350},
    { nombre: "Celular", costo: 10000},
    { nombre: "Laptop", costo: 20000},
    { nombre: "Teclado", costo: 500},
    { nombre: "Audífonos", costo: 1700}
] 

// Metodo find
var encuentraArticulo = articulos.find(function(articulo){
    return articulo.nombre === "Laptop"
});

// For Each. Este método no genera un nuevo array
articulos.forEach(function(articulo){
    console.log(articulo.nombre);
});

// Método Some. Bool
var articulosBaratos = articulos.some(function(articulo){
    return articulo.costo <= 700
});
