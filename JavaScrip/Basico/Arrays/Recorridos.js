// Métodos de recorrido para Arrays

// Creamos un array de objeto 

var articulos = [
    { nombre: "Bici", costo: 3000},
    { nombre: "TV", costo: 3500},
    { nombre: "Libro", costo: 350},
    { nombre: "Celular", costo: 10000},
    { nombre: "Laptop", costo: 20000},
    { nombre: "Teclado", costo: 500},
    { nombre: "Audífonos", costo: 1700}
] 


// Método filter. Este método genera un nuevo Array
var articulosFiltrados = articulos.filter(function(articulo){
    return articulo.costo <= 500
});

// Metodo map. Retorna un nuevo array
var nombreArticulo = articulos.map(function(articulo){
    return articulo.nombre
});
