var objeto = {};

var miAuto = {
    // Atributos
    marca:"Toyota",
    modelo:"Corolla",
    annio: 2020,
    // Métodos
    detalleDelAuto: function(){
        console.log(`Auto ${this.modelo}, ${this.annio}`);
    }
};

// Acceso a los atributos del objeto
console.log(miAuto.marca)

// Acceso a los métodos
consolo.log(miAuto.detalleDelAuto());
