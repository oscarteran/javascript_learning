if (true) {
    console.log("Hola a todos")
} else {
    console.log("Esto es falso")
}

var edad = 18;

if (edad === 18) {
    console.log("Es tu primer votación")
} else if (edad > 18) {
    console.log("No es tu primer votación")
} else {
    console.log("No puedes votar")
}


// Operador terciario
condition ? true : false;

var numero = 1;

var resultado = numero === 1 ? "Es uno" : "No es uno"