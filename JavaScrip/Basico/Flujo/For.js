// Los ciclos for sirven para automatizar tareas

var estudiantes = ["Maria", "Sergio", "Rosa", "Daniel"];

function saludarEstudiantes(estudiante){
    console.log(`Hola ${estudiante}`);
}

for (var i = 0; i < estudiantes.length; i++) {
    console.log(saludarEstudiantes(estudiantes[i]))
}


for (var estudiante of estudiantes) {
    console.log(saludarEstudiantes(estudiante))
}
