# TypeScript

**Author:** Oscar Hernández Terán                      
**Date:** February 13, 2023

This project is a compilation of the information shown in the book:

~~~~
Programming TypeScript
by Boris Cherny
Released May 2019
Publisher(s): O'Reilly Media, Inc.
ISBN: 9781492037651>
~~~~

**Contact**
- [Correo](oscarhdzteran@gmail.com)
- [Linkedin](https://www.linkedin.com/in/oscar-hern%C3%A1ndez-ter%C3%A1n-32b7aa208/)
