function squareOf(n) {
    return n * n
    }
squareOf(2)
// evaluates to 4
squareOf('z')
// evaluates to NaN


function squareOf_n(n: number) {
    return n * n
    }
squareOf(2) // evaluates to 4
squareOf('z') // Error TS2345: Argument of type '"z"' is not assignable of
// parameter of type 'number'.

// ANY
let a: any = 666  // any
let b: any = ['danger']   // any
let c = a + b   // any


// UNKNOWN
let a: unknown = 30 // unknown
let b = a === 123   // boolean
let c = a + 10      // Error TS2571: Object is of type 'unknown'.
if (typeof a === 'number') {
    let d = a + 10  // number
    }


// BOOLEAN
let a = true     // boolean
var b = false    // boolean
const c = true   // true 
let d: boolean = true   // boolean
let e: true = true   // true
let f: true = false  // Error TS2322: Type 'false' is not assignable to type 'true'.



// NUMBER
let a = 1234            // number
var b = Infinity * 0.10 // number
const c = 5678          // 5678
let d = a < b           // boolean
let e: number = 100     // number
let f: 26.218 = 26.218  // 26.218
let g: 26.218 = 10      // Error TS2322: Type '10' is not assignable to type '26.218'.


// STRING1
let a = 'hello'                // string   
var b = 'billy'                // string 
const c = '!'                  // string
let d = a + ' ' + b + c        // string
let e: string = 'zoom'         // string
let f: 'john' = 'john'         // string
let g: 'john' = 'zoe'          // string


// SYMBOL
let a = Symbol('a')           // symbol
let b: symbol = Symbol('b')   // symbol
var c = a === b               // boolean
let d = a + 'x'               // Error TS2469: The '+' operator cannot be applied to type 'symbol'.


// OBJECT
let a = {
    b: 'x'
}              // {b: string}
a.b            // string
    
let b = {
    c: {
        d: 'f'
    }
}              // {c: {d: string}}


// ARRAYS
let a = [1, 2, 3]             // number[]
var b = ['a', 'b']            // string[]
let c: string[] = ['a']       // string[]
let d = [1, 'a']              // (string | number)[]
const e = [2, 'b']            // (string | number)[]

let f = ['red']
f.push('blue')
f.push(true)                  // Error TS2345: Argument of type 'true' is not assignable to parameter of type 'string'.

let g = []                    // any[]
g.push(1)                     // number[]          
g.push('red')                 // (string | number)[]

let h: number[] = []          // number[]
h.push(1)                     // number[]
h.push('red')                 // Error TS2345: Argument of type '"red"' is not assignable to parameter of type 'number'.



// TUPLES 
let a: [number] = [1]
// A tuple of [first name, last name, birth year]
let b: [string, string, number] = ['malcolm', 'gladwell', 1963]

b = ['queen', 'elizabeth', 'ii', 1926]          // Error TS2322: Type 'string' is not assignable to type 'number'.


// ENUMS

enum Language {
    English,
    Spanish,
    Russian
    }

let myFirstLanguage = Language.Russian         // Language
let mySecondLanguage = Language['English']     // Language


enum Color {
    Red = '#c10000',
    Blue = '#007ac1',
    Pink = 0xc10050,       // A hexadecimal literal
    White = 255            // A decimal literal
}
let red = Color.Red        // Color
let pink = Color.Pink      // Color
