// FUNCTIONS IN TYPESCRIPT

function add(a: number, b: number) {
    return a + b
}


// EXPLICITY ANNOTATE
function add(a: number, b: number): number {
    return a + b
}


// Named function
function greet(name: string) {
    return 'hello ' + name
}
// Function expression
let greet2 = function(name: string) {
    return 'hello ' + name
}
// Arrow function expression
let greet3 = (name: string) => {
    return 'hello ' + name
}
// Shorthand arrow function expression
let greet4 = (name: string) =>
'   hello ' + name

// Function constructor
let greet5 = new Function('name', 'return "hello " + name')


// INVOKE A FUNCTION
add(1, 2)           // evaluates to 3
greet('Crystal')    // evaluates to 'hello Crystal'

// OPTIONAL AND DEFAULT PARAMETERS
function log(message: string, userId?: string) {
    let time = new Date().toLocaleTimeString()
    console.log(time, message, userId || 'Not signed in')
}
log('Page loaded')                 // Logs "12:38:31 PM Page loaded Not signed in"
log('User signed in', 'da763be')   // Logs "12:38:31 PM User signed inda763be"


// Optional parameters
function log(message: string, userId = 'Not signed in') {
    let time = new Date().toISOString()
    console.log(time, message, userId)
}
log('User clicked on a button', 'da763be')
log('User signed out')


// REST PARAMETERS
function sum(numbers: number[]): number {
    return numbers.reduce((total, n) => total + n, 0)
}
sum([1, 2, 3]) // evaluates to 6


// Instead of resorting to the unsafe arguments
// magic variable, we can instead use rest parameters to safely make our sum
// function accept any number of arguments:
function sumVariadicSafe(...numbers: number[]): number {
    return numbers.reduce((total, n) => total + n, 0)
}
sumVariadicSafe(1, 2, 3) // evaluates to 6


// CALL, APPLY AND BIND
function add(a: number, b: number): number {
    return a + b
    }
add(10, 20)                    // evaluates to 30
add.apply(null, [10, 20])      // evaluates to 30
add.call(null, 10, 20)         // evaluates to 30
add.bind(null, 10, 20)()       // evaluates to 30


// Generator fuctions
function* createFibonacciGenerator() {
    let a = 0
    let b = 1
    while (true) {
        yield a;
        [a, b] = [b, a + b]
    }
}
let fibonacciGenerator = createFibonacciGenerator() //IterableIterator<number>
fibonacciGenerator.next()// evaluates to {value: 0, done: false}
fibonacciGenerator.next()// evaluates to {value: 1, done: false}
fibonacciGenerator.next()// evaluates to {value: 1, done: false}
fibonacciGenerator.next()// evaluates to {value: 2, done: false}
fibonacciGenerator.next()// evaluates to {value: 3, done: false}
fibonacciGenerator.next()// evaluates to {value: 5, done: false}

function* createNumbers(): IterableIterator<number> {
    let n = 0
    while (1) {
        yield n++
    }
}
let numbers = createNumbers()
numbers.next()// evaluates to {value: 0, done: false}
numbers.next()// evaluates to {value: 1, done: false}
numbers.next()// evaluates to {value: 2, done: false}


// Iterators
let numbers = {
    *[Symbol.iterator]() {
    for (let n = 1; n <= 10; n++) {
        yield n
    }
    }
}
