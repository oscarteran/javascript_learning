# Functions

JavaScript and TypeScript support at least five ways to do this:

```javascript
// Named function
function greet(name: string) {
return 'hello ' + name
}
// Function expression
let greet2 = function(name: string) {
return 'hello ' + name
}
// Arrow function expression
let greet3 = (name: string) => {
return 'hello ' + name
}
// Shorthand arrow function expression
let greet4 = (name: string) =>
'hello ' + name
// Function constructor
let greet5 = new Function('name', 'return "hello " + name')
```

### Optional and default parameters
```javascript
function log(message: string, userId?: string) {
let time = new Date().toLocaleTimeString()
console.log(time, message, userId || 'Not signed in')
}
log('Page loaded') // Logs "12:38:31 PM Page loaded Not signed in"
log('User signed in', 'da763be') // Logs "12:38:31 PM User signed in
da763be"
```

### call, apply and bind
```javascript
function add(a: number, b: number): number {
return a + b
}
add(10, 20) // evaluates to 30
add.apply(null, [10, 20]) // evaluates to 30
add.call(null, 10, 20) // evaluates to 30
add.bind(null, 10, 20)() // evaluates to 30
```

**NOTE:**:
```
TSC FLAG: STRICTBINDCALLAPPLY
To safely use .call , .apply , and .bind in your code, be sure to enable the
strictBindCallApply option in your tsconfig.json (it’s automatically enabled if you already
enabled strict mode). 
```


### Generator Functions
```javascript
function* createFibonacciGenerator() {
let a = 0
let b = 1
while (true) {
yield a;
[a, b] = [b, a + b]
}
}```

